# MercantilFront

## Instalaciones

* Prerequisitos
    - [NPM y Node 10+](https://nodejs.org/es/download/)
    - [Angular Cli 7](https://cli.angular.io/)
    - [Git 2.x](https://git-scm.com/downloads)

## Como comenzar

* Clonar el repositorio localmente
    ```
    git clone git@gitlab.com:lucasgandrada/mercantil-front.git
    ``` 

* Resolver las dependencias ***(requiere conexión a internet)***
    ```
    npm install
    ```

* Generar bundler y levantar el servidor de archivos
    ```
    npm start (o ng serve)
    ```

* Generar build productivo
    ```
    npm build:prod
    ```

## Versiones

* 1.0.0