export const environment = {
  production: true,
  baseEndpointGeo: 'https://apis.datos.gob.ar/georef/api/',
  baseEndpoint: 'https://servicios.qamercantilandina.com.ar/',
};
