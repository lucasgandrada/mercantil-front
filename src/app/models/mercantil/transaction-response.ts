export class TransactionResponse {
    status: number;
    message: string;
    transactionId?: string;
}
