import { Brand } from './mercantil/brand';
import { Version } from './mercantil/version';

export class VehicleData {
    brand: Brand;
    year: Date;
    model: string;
    version: Version;
}
