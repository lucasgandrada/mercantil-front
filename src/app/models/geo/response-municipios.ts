import { Municipio } from './municipio';

export class ResponseMunicipios {
    cantidad: number;
    total: number;
    inicio: number;
    parametros: {};
    municipios: [Municipio];
}
