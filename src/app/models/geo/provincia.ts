import { Centroide } from './centroide';

export class Provincia {
    id: string;
    nombre: string;
    nombre_largo: string;
    iso_nombre: string;
    iso_id: string;
    categoria: 'Provincia';
    centroide: Centroide;
}
