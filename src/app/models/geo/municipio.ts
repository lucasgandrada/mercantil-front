import { Centroide } from './centroide';
import { Provincia } from './provincia';

export class Municipio {
    id: string;
    nombre: string;
    nombre_completo: string;
    categoria: 'Municipio';
    provincia: Provincia;
    centroide: Centroide;
}
