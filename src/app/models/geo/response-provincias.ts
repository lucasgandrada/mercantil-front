import { Provincia } from './provincia';

export class ResponseProvincias {
    cantidad: number;
    total: number;
    inicio: number;
    parametros: {};
    provincias: [Provincia];
}
