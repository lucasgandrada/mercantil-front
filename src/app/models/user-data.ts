import { Provincia } from './geo/provincia';
import { Municipio } from './geo/municipio';

export class UserData {
    dni?: string;
    lastName?: string;
    name?: string;
    email?: string;
    cellPhone?: string;
    phone?: string;
    province?: Provincia;
    municipality?: Municipio;
    address?: string;
    birthDate?: Date;
    user?: string;
    password?: string;
}
