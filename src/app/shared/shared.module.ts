import { NgModule } from '@angular/core';
import { CustomValidators } from './custom-validators';

@NgModule({
  declarations: [],
  exports: [],
  providers: [CustomValidators],
})
export class SharedModule { }
