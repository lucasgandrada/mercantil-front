const numericRegex: RegExp = /^[0-9]+$/;
const dniRegex: RegExp = /^[0-9]{8}$/;
const phoneRegex: RegExp = /^[0-9()+-]+$/;
const passwordRegex = /^(?=.*\d)(?=.*[\u0021-\u002b\u003c-\u0040])(?=.*[A-Z])(?=.*[a-z])\S{8,20}$/;

export {
  numericRegex,
  dniRegex,
  phoneRegex,
  passwordRegex,
};
