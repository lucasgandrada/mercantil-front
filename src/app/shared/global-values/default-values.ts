const dniMinLength = 7;
const dniMaxLength = 8;
const phoneMinLength = 10;
const phoneMaxLength = 10;
const passwordMinLength = 8;
const passwordMaxLength = 20;

export {
  passwordMinLength,
  passwordMaxLength,
  dniMinLength,
  dniMaxLength,
  phoneMinLength,
  phoneMaxLength,
};
