import { FormGroup, AbstractControl } from '@angular/forms';
import { CustomValidators } from './custom-validators';
export abstract class BaseFormComponent {
  form: FormGroup;
  submitted = false;

  get controls() {
    return this.form.controls;
  }

  hasError(control: AbstractControl, error?: string): boolean {
    return CustomValidators.hasError(control, error);
  }

  hasAnyError(control: AbstractControl, errors: string[]): boolean {
    return CustomValidators.hasAnyError(control, errors);
  }

  getDescriptionFn(key: string) {
    return (item: any) => (item ? item[key] : '');
  }

  compareItems(i1, i2) {
    return i1 && i2 && i1.id === i2.id;
  }

}
