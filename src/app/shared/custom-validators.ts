import {
  AbstractControl,
  FormGroup,
  FormControl,
} from '@angular/forms';
import { numericRegex, phoneRegex, dniRegex, passwordRegex } from './global-values/regex';
import { passwordMinLength, passwordMaxLength, phoneMinLength, phoneMaxLength } from './global-values/default-values';



export class CustomValidators {
  static compareValidator(controlName: string, compareControlName: string) {
    return (formGroup: FormGroup): void => {
      const control = formGroup.controls[controlName];
      const compareControl = formGroup.controls[compareControlName];

      if (compareControl.errors && !compareControl.errors.compare) {
        return;
      }

      if (control.value !== compareControl.value) {
        compareControl.setErrors({ compare: true });
      } else {
        compareControl.setErrors(null);
      }
    };
  }


  static phone(control: AbstractControl): { [key: string]: boolean } | null {
    if (!control.value) {
      return null;
    }

    let response = {};

    if (!phoneRegex.test(control.value)) {
      response = { ...response, phone: true };
    }

    if (control.value.length < phoneMinLength) {
      response = { ...response, minlength: true };
    } else if (control.value.length > phoneMaxLength) {
      response = { ...response, maxlength: true };
    }

    return response;
  }


  static dni(control: AbstractControl): { [key: number]: boolean } | null {
    if (!control.value) {
      return null;
    }

    let response = {};

    if (!dniRegex.test(control.value)) {
      response = { ...response, dni: true };
    }

    return response;
  }

  static number(control: AbstractControl): { [key: string]: boolean } | null {
    if (!control.value) {
      return null;
    }

    let response = {};

    if (!numericRegex.test(control.value)) {
      response = { ...response, number: true };
    }

    return response;
  }


  static password(control: AbstractControl): { [key: string]: boolean } | null {
    if (!control.value) {
      return null;
    }

    let response = {};

    if (control.value.length < passwordMinLength) {
      return { ...response, minlength: true };
    } else if (control.value.length > passwordMaxLength) {
      response = { ...response, maxlength: true };
    } else if (!passwordRegex.test(control.value)) {
      response = { ...response, pattern: true };
    }

    return response;
  }

  static noWhitespace(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { required: true };
  }


  static hasError(control: AbstractControl, error?: string): boolean {
    if (error) {
      return (control.dirty || control.touched) && control.hasError(error);
    }

    return (control.dirty || control.touched) && control.invalid;
  }

  static hasAnyError(control: AbstractControl, errors: string[]): boolean {
    return (control.dirty || control.touched) && errors.some((error) => control.hasError(error));
  }
}
