import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserDataComponent } from './pages/user-data/user-data.component';
import { AppComponent } from './app.component';
import { VehicleDataComponent } from './pages/vehicle-data/vehicle-data.component';
import { CoveragesDataComponent } from './pages/coverages-data/coverages-data.component';
import { ConfirmDataComponent } from './pages/confirm-data/confirm-data.component';
import { SuccessDataComponent } from './pages/success-data/success-data.component';

const routes: Routes = [
  {
    path: '',
    component: AppComponent,
    children: [
      { path: '', component: UserDataComponent },
      { path: 'vehicle', component: VehicleDataComponent },
      { path: 'coverages', component: CoveragesDataComponent },
      { path: 'confirm', component: ConfirmDataComponent },
      { path: 'success', component: SuccessDataComponent },
      { path: '**', redirectTo: '', pathMatch: 'full' },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
