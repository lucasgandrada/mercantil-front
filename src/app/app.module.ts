import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserDataComponent } from './pages/user-data/user-data.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './shared/material-module';
import { ServicesModule } from './services/services.module';
import { HttpClientModule } from '@angular/common/http';
import { VehicleDataComponent } from './pages/vehicle-data/vehicle-data.component';
import { CoveragesDataComponent } from './pages/coverages-data/coverages-data.component';
import { ConfirmDataComponent } from './pages/confirm-data/confirm-data.component';
import { SuccessDataComponent } from './pages/success-data/success-data.component';
import { SortPipe } from './shared/pipes/sort.pipe';

@NgModule({
  declarations: [
    AppComponent,
    UserDataComponent,
    VehicleDataComponent,
    CoveragesDataComponent,
    ConfirmDataComponent,
    SuccessDataComponent,
    SortPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    ServicesModule,
  ],
  providers: [SortPipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
