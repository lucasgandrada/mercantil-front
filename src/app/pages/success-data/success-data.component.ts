import { Component, OnInit } from '@angular/core';
import { BaseFormComponent } from 'src/app/shared/base-form-component';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { InsuredService } from 'src/app/services/insured.service';
import { TransactionResponse } from 'src/app/models/mercantil/transaction-response';

@Component({
  selector: 'app-success-data',
  templateUrl: './success-data.component.html',
  styleUrls: ['./success-data.component.scss']
})
export class SuccessDataComponent extends BaseFormComponent implements OnInit {

  transactionResponse: TransactionResponse;

  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private insuredService: InsuredService, ) {
    super();
  }

  ngOnInit() {
    this.form = this.formBuilder.group({});
    if (this.insuredService.transactionResponse) {
      this.transactionResponse = this.insuredService.transactionResponse;
    }
  }

  init() {
    this.insuredService.reset();
    this.router.navigate(['']);
  }


}
