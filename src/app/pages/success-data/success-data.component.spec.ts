import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccessDataComponent } from './success-data.component';

describe('SuccessDataComponent', () => {
  let component: SuccessDataComponent;
  let fixture: ComponentFixture<SuccessDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuccessDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuccessDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
