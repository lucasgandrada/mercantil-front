import { Component, OnInit } from '@angular/core';
import { InsuredService } from 'src/app/services/insured.service';
import { Router } from '@angular/router';
import { BaseFormComponent } from 'src/app/shared/base-form-component';
import { FormBuilder } from '@angular/forms';
import { UserData } from 'src/app/models/user-data';
import { VehicleData } from 'src/app/models/vehicle-data';
import { Coverage } from 'src/app/models/mercantil/coverage';
import { MercantilService } from 'src/app/services/mercantil.service';

@Component({
  selector: 'app-confirm-data',
  templateUrl: './confirm-data.component.html',
  styleUrls: ['./confirm-data.component.scss']
})
export class ConfirmDataComponent extends BaseFormComponent implements OnInit {

  userData: UserData;
  vehicleData: VehicleData;
  coverage: Coverage;
  isValid: boolean;

  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private insuredService: InsuredService,
    private mercantilService: MercantilService, ) {
    super();
  }

  ngOnInit() {
    this.isValid = false;

    this.form = this.formBuilder.group({});

    if (this.insuredService.userData && this.insuredService.vehicleData && this.insuredService.coverage) {
      this.userData = this.insuredService.userData;
      this.userData = this.insuredService.userData;
      this.coverage = this.insuredService.coverage;
      this.isValid = true;
    }
  }

  back(): void {
    this.router.navigate(['coverages']);
  }

  confirm(): void {
    this.mercantilService.process().subscribe(
      response => {
        this.insuredService.transactionResponse = response;
        this.router.navigate(['success']);
      },
      error => {
      }
    );
  }

}
