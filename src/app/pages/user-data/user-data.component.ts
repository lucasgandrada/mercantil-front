import { Component, OnInit } from '@angular/core';
import { BaseFormComponent } from '../../shared/base-form-component';
import { FormBuilder, Validators } from '@angular/forms';
import { GeoService } from '../../services/geo.service';
import { Provincia } from '../../models/geo/provincia';
import { Municipio } from '../../models/geo/municipio';
import { MercantilService } from '../../services/mercantil.service';
import { debounceTime, switchMap } from 'rxjs/operators';
import { CustomValidators } from '../../shared/custom-validators';
import { Router } from '@angular/router';
import { UserData } from '../../models/user-data';
import { InsuredService } from '../../services/insured.service';
@Component({
  selector: 'app-user-data',
  templateUrl: './user-data.component.html',
  styleUrls: ['./user-data.component.scss']
})
export class UserDataComponent extends BaseFormComponent implements OnInit {
  provinces: [Provincia];
  municipalities: [Municipio];
  minDate: Date;
  maxDate: Date;

  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private geoService: GeoService,
    private mercantilService: MercantilService,
    private insuredService: InsuredService, ) {

    super();
    const currentYear = new Date().getFullYear();
    const currentMonth = new Date().getMonth();
    const currentDay = new Date().getDate();
    this.minDate = new Date(currentYear - 99, currentMonth, currentDay);
    this.maxDate = new Date(currentYear - 18, currentMonth, currentDay);

  }

  ngOnInit() {
    this.getProvinces();

    this.form = this.formBuilder.group({
      dni: [
        this.insuredService.userData.dni,
        [
          Validators.required,
          Validators.minLength(7),
          Validators.maxLength(8),
          Validators.pattern('^[0-9]*$'),
        ]
      ],
      lastName: [
        this.insuredService.userData.lastName,
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(15),
          Validators.pattern('^[a-zA-ZáéíóúÁÉÍÓÚ ]*$'),
        ]
      ],
      name: [
        this.insuredService.userData.name,
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(15),
          Validators.pattern('^[a-zA-ZáéíóúÁÉÍÓÚ ]*$'),
        ]
      ],
      email: [
        this.insuredService.userData.email,
        [
          Validators.required,
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
        ]
      ],
      cellPhone: [
        this.insuredService.userData.cellPhone,
        [
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(10),
          Validators.pattern('^[1-9]{1}[0-9]{9}$'),

        ]
      ],
      phone: [
        this.insuredService.userData.phone,
        [
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(10),
          Validators.pattern('^[1-9]{1}[0-9]{9}$'),

        ]
      ],
      province: [
        this.insuredService.userData.province,
        [
          Validators.required,
        ]
      ],
      municipality: [
        this.insuredService.userData.municipality,
        [
          Validators.required,
        ]
      ],
      address: [
        this.insuredService.userData.address,
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(30),
          Validators.pattern('^[a-zA-ZáéíóúÁÉÍÓÚ0-9 ]*$'),
        ]
      ],
      birthDate: [
        this.insuredService.userData.birthDate,
        [
          Validators.required,
        ]
      ],
      user: [
        this.insuredService.userData.user,
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(30),
          Validators.pattern('^[a-z0-9.]*$'),
        ]
      ],
      password: [
        this.insuredService.userData.password,
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(20),
          CustomValidators.password,
        ]
      ],
    });

    if (this.insuredService.userData.province) {
      this.getMunicipalities();
    }

    this.searchUser();

  }

  getProvinces(): void {
    this.geoService.getProvinces().subscribe(
      response => {
        if (response.cantidad > 0) {
          this.provinces = response.provincias;
        }
      },
      error => {
      }
    );
  }

  getMunicipalities(): void {
    if (this.controls.province.value) {
      const province = this.controls.province.value;
      this.geoService.getMunicipalities(province).subscribe(
        response => {
          if (response.cantidad > 0) {
            this.municipalities = response.municipios;
          }
        },
        error => {
        }
      );
    }
  }

  onProvinceChanged(): void {
    this.getMunicipalities();
  }


  searchUser(): void {
    this.controls.user.valueChanges.pipe(
      debounceTime(500),
      switchMap(user => {
        return this.mercantilService.getUsers(user);
      })
    ).subscribe(response => {
      if (response) {
        this.form.controls['user'].setErrors({ userExist: true });
      }
    });
  }


  onSubmit(): void {
    if (this.form.invalid) {
      return;
    }

    const userData: UserData = new UserData();
    userData.dni = this.controls.dni.value;
    userData.lastName = this.controls.lastName.value;
    userData.name = this.controls.name.value;
    userData.email = this.controls.email.value;
    userData.cellPhone = this.controls.cellPhone.value;
    userData.phone = this.controls.phone.value;
    userData.province = this.controls.province.value;
    userData.municipality = this.controls.municipality.value;
    userData.address = this.controls.address.value;
    userData.birthDate = this.controls.birthDate.value;
    userData.user = this.controls.user.value;
    userData.password = this.controls.password.value;

    this.insuredService.userData = userData;


    this.router.navigate(['vehicle']);
  }

}
