import { Component, OnInit } from '@angular/core';
import { BaseFormComponent } from '../../shared/base-form-component';
import { FormBuilder, Validators } from '@angular/forms';
import { MercantilService } from '../../services/mercantil.service';
import { Brand } from '../../models/mercantil/brand';
import { MatDatepickerInputEvent } from '@angular/material';
import { Version } from '../../models/mercantil/version';
import { Router } from '@angular/router';
import { InsuredService } from '../../services/insured.service';
import { VehicleData } from '../../models/vehicle-data';

@Component({
  selector: 'app-vehicle-data',
  templateUrl: './vehicle-data.component.html',
  styleUrls: ['./vehicle-data.component.scss']
})
export class VehicleDataComponent extends BaseFormComponent implements OnInit {

  brands: [Brand];
  minDate: Date;
  maxDate: Date;
  models: [string];
  versions: [Version];


  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private mercantilService: MercantilService,
    private insuredService: InsuredService, ) {
    super();
    const currentYear = new Date().getFullYear();
    const currentMonth = new Date().getMonth();
    const currentDay = new Date().getDate();
    this.minDate = new Date(currentYear - 20, currentMonth, currentDay);
    this.maxDate = new Date(currentYear, currentMonth, currentDay);
  }

  ngOnInit() {

    this.getBrands();

    this.form = this.formBuilder.group({
      brand: [
        this.insuredService.vehicleData.brand,
        [
          Validators.required,
        ]
      ],
      year: [
        this.insuredService.vehicleData.year,
        [
          Validators.required,
        ]
      ],
      model: [
        this.insuredService.vehicleData.model,
        [
          Validators.required,
        ]
      ],
      version: [this.insuredService.vehicleData.version],

    });

    if (this.insuredService.vehicleData.brand && this.insuredService.vehicleData.year) {
      this.getModels();
    }

    if (this.insuredService.vehicleData.brand && this.insuredService.vehicleData.year && this.insuredService.vehicleData.model) {
      this.getVersions();
    }

  }

  getBrands(): void {
    this.mercantilService.getBrands().subscribe(
      response => {
        this.brands = response;
      },
      error => {
      }
    );
  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.getModels();
  }

  getModels(): void {
    if (this.controls.brand.valid && this.controls.year.valid) {
      this.models = null;
      this.versions = null;
      const brand = this.controls.brand.value;
      const date = this.controls.year.value;

      this.mercantilService.getModels(brand, date).subscribe(
        response => {
          this.models = response;
        },
        error => {
        }
      );
    }
  }

  getVersions(): void {
    if (this.controls.brand.valid && this.controls.year.valid && this.controls.model.valid) {
      this.versions = null;
      const brand = this.controls.brand.value;
      const date = this.controls.year.value;
      const model = this.controls.model.value;

      this.mercantilService.getVersions(brand, date, model).subscribe(
        response => {
          this.versions = response;
        },
        error => {
        }
      );
    }
  }

  back(): void {
    this.router.navigate(['']);
  }

  onSubmit(): void {
    if (this.form.invalid) {
      return;
    }


    const vehicleData: VehicleData = new VehicleData();
    vehicleData.brand = this.controls.brand.value;
    vehicleData.year = this.controls.year.value;
    vehicleData.model = this.controls.model.value;
    vehicleData.version = this.controls.version.value;

    this.insuredService.vehicleData = vehicleData;

    this.router.navigate(['coverages']);

  }



}
