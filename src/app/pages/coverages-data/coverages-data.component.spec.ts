import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoveragesDataComponent } from './coverages-data.component';

describe('CoveragesDataComponent', () => {
  let component: CoveragesDataComponent;
  let fixture: ComponentFixture<CoveragesDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoveragesDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoveragesDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
