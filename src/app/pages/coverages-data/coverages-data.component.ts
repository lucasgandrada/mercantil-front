import { Component, OnInit, HostListener } from '@angular/core';
import { BaseFormComponent } from '../../shared/base-form-component';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { MercantilService } from '../../services/mercantil.service';
import { InsuredService } from '../../services/insured.service';
import { Coverage } from 'src/app/models/mercantil/coverage';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatTableDataSource } from '@angular/material';
import { SortPipe } from 'src/app/shared/pipes/sort.pipe';

@Component({
  selector: 'app-coverages-data',
  templateUrl: './coverages-data.component.html',
  styleUrls: ['./coverages-data.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class CoveragesDataComponent extends BaseFormComponent implements OnInit {
  dataSource: any;
  columnsToDisplay: string[] =
    [
      'numero',
      'costo',
      'franquicia',
      'titulo',
      'puntaje'];
  expandedElement: Coverage | null;


  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private mercantilService: MercantilService,
    private insuredService: InsuredService,
    private sortPipe: SortPipe) {
    super();
  }
  @HostListener('window:resize')
  resize(): void {
    if (window.innerWidth <= 955) {
      if (window.innerWidth <= 760) {
        this.columnsToDisplay = ['numero', 'titulo'];
      } else {
        this.columnsToDisplay = ['numero', 'titulo', 'puntaje'];
      }
    } else {
      this.columnsToDisplay = ['numero', 'costo', 'titulo', 'puntaje'];
    }
  }

  ngOnInit() {
    this.getCoverages();

    this.form = this.formBuilder.group({});

    if (this.insuredService.coverage) {
      this.expandedElement = this.insuredService.coverage;
    }
  }


  getCoverages(): void {
    this.mercantilService.getCoverages().subscribe(
      response => {
        response = this.sortPipe.transform(response, 'desc', 'puntaje');
        this.dataSource = new MatTableDataSource<Coverage>(response);
      },
      error => {
      }
    );
  }

  back(): void {
    this.router.navigate(['vehicle']);
  }

  onSubmit(): void {
    if (this.form.invalid) {
      return;
    }
    this.insuredService.coverage = this.expandedElement;

    this.router.navigate(['confirm']);

  }

}
