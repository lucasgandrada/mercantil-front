import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ResponseProvincias } from '../models/geo/response-provincias';
import { Observable } from 'rxjs';
import { ResponseMunicipios } from '../models/geo/response-municipios';
import { Provincia } from '../models/geo/provincia';

@Injectable({
  providedIn: 'root',
})
export class GeoService {

  fullPath: string = null;

  constructor(private _httpClient: HttpClient) {
    this.fullPath = `${environment.baseEndpointGeo}`;
  }

  getProvinces(): Observable<ResponseProvincias> {
    return this._httpClient.get<ResponseProvincias>(`${this.fullPath}provincias`);
  }

  getMunicipalities(province: Provincia): Observable<ResponseMunicipios> {
    return this._httpClient.get<ResponseMunicipios>(`${this.fullPath}municipios?provincia=${province.id}&campos=id,nombre&max=135`);
  }

}
