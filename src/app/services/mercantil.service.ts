import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Brand } from '../models/mercantil/brand';
import { Version } from '../models/mercantil/version';
import { Coverage } from '../models/mercantil/coverage';
import { TransactionResponse } from '../models/mercantil/transaction-response';

@Injectable({
  providedIn: 'root',
})
export class MercantilService {


  pathMock = 'api_mock_frontend/v1/';
  path = 'api/v1/';
  fullPath: string = null;

  constructor(private _httpClient: HttpClient) {
    this.fullPath = `${environment.baseEndpoint}`;
  }

  getUsers(user: string): Observable<boolean> {
    return this._httpClient.get<boolean>(`${this.fullPath}${this.pathMock}usuarios?nombre=${user}`);
  }

  getBrands(): Observable<[Brand]> {
    return this._httpClient.get<[Brand]>(`${this.fullPath}${this.path}vehiculos/marcas`);
  }

  getModels(brand: Brand, date: Date): Observable<[string]> {
    const cod = brand.codigo;
    const year = date.getFullYear();
    return this._httpClient.get<[string]>(`${this.fullPath}${this.path}vehiculos/marcas/${cod}/${year}`);
  }

  getVersions(brand: Brand, date: Date, model: string): Observable<[Version]> {
    const cod = brand.codigo;
    const year = date.getFullYear();
    return this._httpClient.get<[Version]>(`${this.fullPath}${this.path}vehiculos/marcas/${cod}/${year}/${model}`);
  }

  getCoverages(): Observable<[Coverage]> {
    return this._httpClient.get<[Coverage]>(`${this.fullPath}${this.pathMock}coberturas`);
  }

  process(): Observable<TransactionResponse> {
    const transactionResponse: TransactionResponse = {
      status: 200,
      message: 'OK',
      transactionId: '12345678',
    };
    return of(transactionResponse);
  }
}
