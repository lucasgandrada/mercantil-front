import { NgModule } from '@angular/core';
import { GeoService } from './geo.service';
import { MercantilService } from './mercantil.service';
import { InsuredService } from './insured.service';

@NgModule({
  providers: [
    { provide: Window, useValue: window },
    GeoService,
    MercantilService,
    InsuredService,
  ],
})
export class ServicesModule {
}
