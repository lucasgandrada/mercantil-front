import { TestBed } from '@angular/core/testing';

import { MercantilService } from './mercantil.service';

describe('MercantilService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MercantilService = TestBed.get(MercantilService);
    expect(service).toBeTruthy();
  });
});
