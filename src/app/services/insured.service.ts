import { Injectable } from '@angular/core';
import { UserData } from '../models/user-data';
import { VehicleData } from '../models/vehicle-data';
import { Coverage } from '../models/mercantil/coverage';
import { TransactionResponse } from '../models/mercantil/transaction-response';

@Injectable({
  providedIn: 'root',
})
export class InsuredService {

  private _userData: UserData = new UserData();
  private _vehicleData: VehicleData = new VehicleData();
  private _coverage: Coverage;
  transactionResponse: TransactionResponse;

  constructor() { }

  get userData() { return this._userData; }
  set userData(userData: UserData) {
    this._userData = userData;
  }

  get vehicleData() { return this._vehicleData; }
  set vehicleData(vehicleData: VehicleData) {
    this._vehicleData = vehicleData;
  }

  get coverage() { return this._coverage; }
  set coverage(coverage: Coverage) {
    this._coverage = coverage;
  }


  reset() {
    this._userData = new UserData();
    this._vehicleData = new VehicleData();
    this._coverage = null;
    this.transactionResponse = null;
  }


}
